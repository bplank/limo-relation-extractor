

#########################################

#SET THE DIRECTORY

#export LIMODIR=$(HOME)/tools/limo-rel-ACL2013

SVM_DIR=$(LIMODIR)/svm_semantic
#####

DATADIR=data/ace2005

WACKYMATRIX=$(LIMODIR)/matrix/lsa_ukwac_all_ace2005vocabOnly.txt/matrix.bin
EMPTYMATRIX=$(LIMODIR)/matrix/empty/matrix.bin


D=true
GR=type

## configuration files
CHARNIAK_ZHANG=conf-train-test-charniak-zhang-ry.xml
CHARNIAK_ZHANG_WC=conf-train-test-charniak-zhang-ry-wc.xml
CHARNIAK_ZHANG_SEMTK=conf-train-test-charniak-zhang-ry-semkernel.xml
CHARNIAK_ZHANG_WC_SEMTK=conf-train-test-charniak-zhang-ry-semkernel-wc.xml


CHARNIAK_ZHANG_EXAMPLE=conf-train-test-charniak-zhang-ry-example.xml

#########################################


example:
	nohup make t5-tree-c-svm-light-semtk F=1 TREETYPE=PET c=2.4 CONF=$(CHARNIAK_ZHANG_EXAMPLE) TRAINDIR=nw_bn_example-RY TESTDIR=bc_example-RY  > nohup.example-baseline-charniak.$(D).f1.PET.c2.4 



### baselines                                                                                                                                                                                                                                                                     
baseline-pet:
	nohup make t5-tree-c-svm-light-semtk F=1 TREETYPE=PET c=2.4 CONF=$(CHARNIAK_ZHANG) TRAINDIR=nw_bn-RY TESTDIR=bc1-RY  > nohup.bc1-baseline-charniak.$(D).f1.PET.c2.4 
	nohup make t5-tree-c-svm-light-semtk F=1 TREETYPE=PET c=2.4 CONF=$(CHARNIAK_ZHANG) TRAINDIR=nw_bn-RY TESTDIR=cts-RY > nohup.cts-baseline-charniak.$(D).f1.PET.c2.4 
	nohup make t5-tree-c-svm-light-semtk F=1 TREETYPE=PET c=2.4 CONF=$(CHARNIAK_ZHANG) TRAINDIR=nw_bn-RY TESTDIR=wl-RY  > nohup.wl-baseline-charniak.$(D).f1.PET.c2.4

baseline-bow:
	nohup make t5-tree-c-svm-light-semtk F=1 TREETYPE=BOW c=2.4 CONF=$(CHARNIAK_ZHANG) TRAINDIR=nw_bn-RY TESTDIR=cts-RY  > nohup.cts-baseline-charniak.$(D).f1.BOW.c2.4 &
	nohup make t5-tree-c-svm-light-semtk F=1 TREETYPE=BOW c=2.4 CONF=$(CHARNIAK_ZHANG) TRAINDIR=nw_bn-RY TESTDIR=bc1-RY  > nohup.bc1-baseline-charniak.$(D).f1.BOW.c2.4  &
	nohup make t5-tree-c-svm-light-semtk F=1 TREETYPE=BOW c=2.4 CONF=$(CHARNIAK_ZHANG) TRAINDIR=nw_bn-RY TESTDIR=wl-RY  > nohup.wl-baseline-charniak.$(D).f1.BOW.c2.4  
	nohup make t5-tree-c-svm-light-semtk F=1 TREETYPE=BOW_M c=2.4 CONF=$(CHARNIAK_ZHANG) TRAINDIR=nw_bn-RY TESTDIR=bc1-RY  > nohup.bc1-baseline-charniak.$(D).f1.BOW_M.c2.4 &
	nohup make t5-tree-c-svm-light-semtk F=1 TREETYPE=BOW_M c=2.4 CONF=$(CHARNIAK_ZHANG) TRAINDIR=nw_bn-RY TESTDIR=cts-RY  > nohup.cts-baseline-charniak.$(D).f1.BOW_M.c2.4 &
	nohup make t5-tree-c-svm-light-semtk F=1 TREETYPE=BOW_M c=2.4 CONF=$(CHARNIAK_ZHANG) TRAINDIR=nw_bn-RY TESTDIR=wl-RY  > nohup.wl-baseline-charniak.$(D).f1.BOW_M.c2.4 &

### before running this, adjust the paths in the CHARNIAK_ZHANG_WC_SEMTK file!
system:
	nohup make t5-tree-c-svm-light-semtk-withMatrix T=5 F=1 TREETYPE=PET+PETwc+PETlsa c=2.4 CONF=$(CHARNIAK_ZHANG_WC_SEMTK) TRAINDIR=nw_bn-RY TESTDIR=bc1-RY MATRIX=$(WACKYMATRIX) > nohup.bc1-charniak.$(D).f1.PET.F1.c2.4.PET+PETwc+PETlsa 
	nohup make t5-tree-c-svm-light-semtk-withMatrix T=5 F=1 TREETYPE=PET+PETwc+PETlsa c=2.4 CONF=$(CHARNIAK_ZHANG_WC_SEMTK) TRAINDIR=nw_bn-RY TESTDIR=cts-RY MATRIX=$(WACKYMATRIX) > nohup.cts-charniak.$(D).f1.PET.F1.c2.4.PET+PETwc+PETlsa 
	nohup make t5-tree-c-svm-light-semtk-withMatrix T=5 F=1 TREETYPE=PET+PETwc+PETlsa c=2.4 CONF=$(CHARNIAK_ZHANG_WC_SEMTK) TRAINDIR=nw_bn-RY TESTDIR=wl-RY MATRIX=$(WACKYMATRIX) > nohup.wl-charniak.$(D).f1.PET.F1.c2.4.PET+PETwc+PETlsa 

###

t5-tree-c-svm-light-semtk:
	$(MAKE) all-c-onlyT-svm-sptk F=$(F) L=0.4 TRAINDIR=$(TRAINDIR) TESTDIR=$(TESTDIR) D=$(D) GR=$(GR) TREETYPE=$(TREETYPE) SVMDIR=$(SVM_DIR)/ CONF=$(CONF) c=$(c) 

t5-tree-c-svm-light-semtk-withMatrix:
	$(MAKE) all-c-onlyT-svm-sptk-withMatrix T=$(T) F=$(F) L=0.4 TRAINDIR=$(TRAINDIR) TESTDIR=$(TESTDIR) D=$(D) GR=$(GR) TREETYPE=$(TREETYPE) SVMDIR=$(SVM_DIR) CONF=$(CONF) c=$(c) MATRIX=$(MATRIX)




##############################################

all-c-onlyT-svm-sptk:
	$(MAKE) run-train-test OUTDIR=run/semtk-$(TREETYPE)-t5-F$(F)-L$(L)-c$(c)-GR$(GR)-Tr$(TRAINDIR)-Te$(TESTDIR)-$(CONF)-onlyTree PARAMS="-t 5 -F $(F) -L $(L) -c $(c) -m 10000 -u $(EMPTYMATRIX)" FEATS=configuration/features-$(TREETYPE).xml GR=$(GR) CONF=$(CONF)
	$(MAKE) eval OUTDIR=run/semtk-$(TREETYPE)-t5-F$(F)-L$(L)-c$(c)-GR$(GR)-Tr$(TRAINDIR)-Te$(TESTDIR)-$(CONF)-onlyTree FEATS=configuration/features-$(TREETYPE).xml GR=$(GR) CONF=$(CONF)


all-c-onlyT-svm-sptk-withMatrix:
	$(MAKE) run-train-test OUTDIR=run/semtk-$(TREETYPE)-t5-F$(F)-L$(L)-c$(c)-GR$(GR)-Tr$(TRAINDIR)-Te$(TESTDIR)-$(CONF)-onlyTree PARAMS="-t $(T) -F $(F) -L $(L) -c $(c) -m 8000 -u $(MATRIX)" FEATS=configuration/features-$(TREETYPE).xml GR=$(GR) CONF=$(CONF)
	$(MAKE) eval OUTDIR=run/semtk-$(TREETYPE)-t5-F$(F)-L$(L)-c$(c)-GR$(GR)-Tr$(TRAINDIR)-Te$(TESTDIR)-$(CONF)-onlyTree FEATS=configuration/features-$(TREETYPE).xml GR=$(GR) CONF=$(CONF)
	cat run/semtk-$(TREETYPE)-t5-F$(F)-L$(L)-c$(c)-GR$(GR)-Tr$(TRAINDIR)-Te$(TESTDIR)-$(CONF)-onlyTree/models/*stderr*


######################################################################


FEATS=configuration/features-$(PET)+linear$(FE).xml
TMP=~/tmp
#D=true #(directed)
#GR=type #or subtype
WRAPPER=./bin/wrapper.sh
run-train-test:
	echo "TRAINDIR:", $(TRAINDIR)
	echo "TESTDIR:",$(TESTDIR)
	rm -rf $(OUTDIR)
	$(WRAPPER) training $(DATADIR)/$(TRAINDIR) $(DATADIR)/$(TESTDIR) $(SVMDIR) "$(PARAMS)" $(OUTDIR) $(FEATS) $(D) $(GR) configuration/$(CONF)
	$(WRAPPER) test $(DATADIR)/$(TRAINDIR) $(DATADIR)/$(TESTDIR) $(SVMDIR) "$(PARAMS)" $(OUTDIR) $(FEATS) $(D) $(GR) configuration/$(CONF)

eval:
	echo "ALL"
	python3 eval.py $(OUTDIR)/test/out.gold.idx  $(OUTDIR)/test/out.predicted.class






